# Second class, 30 September 2024

Spaces of words Σ^ω

Büchi automata

Acceptance condition: Infty(run) ∩ F ≠ ∅ [Finkbeiner: Definition 2.4]

Büchi-recognizable languages

Review of regular languages; ω-regular languages [Finkbeiner §3.1, §3.2]

Theorem 1. Union of Büchi-recognizable languages are Büchi-recognizable [Finkbeiner: Theorem 3.2]

Theorem 2. Intersection of Büchi-recognizable languages are Büchi-recognizable [Finkbeiner: Theorem 3.3]

Finkbeiner Chapter 2, 3.
