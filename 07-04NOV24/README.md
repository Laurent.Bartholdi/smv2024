# Seventh class, 4 November 2024

S1S logic: second-order (quantifying on elements and subsets) with one successor function.

Interpretation in naturals. Examples: ≤ on terms, ⊆ on subsets; can express emptiness, finiteness, etc. Operations ∩, etc.

Theorem. If ϕ is a QPTL formula, then there exists an S1S formula accepting the same language.

Theorem. If ϕ is an S1S formula, then there exists a Büchi automaton accepting the same language.

Corollary. It is decidable, given an S1S formula, whether it holds universally (i.e. in every interpretation). Even better, the language of those interpretations at which it holds is Büchi-recognizable, and a Büchi automaton recognizing it can be effectively computed.

Finkbeiner Chapter 7.
