# Twelvth class, 9 December 2024

Games. Reachability, Büchi, Parity, ...

(Memoryless) determined games.

Theorem: Reachability games are memoryless determined.

Finkbeiner chapter 10.
