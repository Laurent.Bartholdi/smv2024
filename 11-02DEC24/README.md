# Eleventh class, 2 December 2024

Tree automata, S2S. Important S2S formulas: ≤, PrefixClosed(X), PrefixClosure(X,Y), Chain(X), Path(X), IsInfinite(X).

Theorem: given an S2S formula, there exists a parity tree automaton that recognizes its satisfiable interpretations. Given a parity tree automaton, there is an S2S formula recognizing its language.

CTL: a temporal logic with state and path formulas. Can be represented in S2S, and can express "whatever we want" on Kripke structures.

Finkbeiner chapter 10.
