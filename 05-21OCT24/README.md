# Fifth class, 21 October 2024

Decidability properties of Büchi-recognizable languages: we have algorithms for intersection, union, complementation, and testing emptiness / equality / inclusion.

Kripke structures: what is used to describe programs. Really a special kind of Büchi automaton, with alphabet 2^AP.

Linear time properties: subsets of (2^AP)^ω that express properties of programs.

Definition. Software Verification ≡ testing a linear time property against a Büchi structure. Decidable if the property is Büchi-recognizable (which is often the case in practice)

Linear Time Logic (Pnueli '77). Definition, with its syntax and semantics.

Theorem. { α : α ⊨ ϕ } is Büchi-recognizable for every LTL formula ϕ [not proven -- this will happen next week]

Finkbeiner Chapter 7.
