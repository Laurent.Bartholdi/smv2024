#include <pthread.h>
#include <unistd.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#define N 5

/* run with: verify philo-2.c */

/* completely wrong: no mutexes / semaphors; deadlock */

int eats[N];
int chopsticks[N];

void *philosopher(void *arg)
{
  int id, nextid;
  
  id = (long) arg;
  nextid = (id + 1) % N;

  eats[id] = 0;
  
  while (1) {
    usleep(1000); /* think */
    
    while (chopsticks[id] == 1) usleep(10);
    chopsticks[id] = 1;
    while (chopsticks[nextid] == 1);
    chopsticks[nextid] = 1;

    eats[id]++;
    usleep(1000); /* eat */
    
    chopsticks[id] = 0;
    chopsticks[nextid] = 0;
  }
}

int main(void)
{
  pthread_t t[N];
  long i;
  
  for (i = 0; i < N; i++)
    chopsticks[i] = eats[i] = 0;
  
  for (i = 0; i < N; i++)
    pthread_create(&t[i], 0, philosopher, (void *) i);

  usleep(1000000);
  
  for (i = 0; i < N; i++)
    assert(eats[i] > 100);

  return 0;
}
