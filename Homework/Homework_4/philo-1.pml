#define N 5

bit chopsticks[N];
bit eats[N];

init {
  atomic {
    byte i = 0;
    do
    ::(i < N) -> run philosopher(i); i++;
    ::else -> break;
    od;
  }
}

proctype philosopher(byte id) {
  eats[id] = 0;
thinking:
  atomic { chopsticks[id] == 0 -> chopsticks[id] = 1; }
  atomic { chopsticks[(id + 1)%N] == 0; -> chopsticks[(id + 1)%N] = 1; }
  eats[id] = 1;
eating:
  eats[id] = 0;
  chopsticks[(id + 1)%N] = 0; 
  chopsticks[id] = 0;
  goto thinking;
}

ltl ltl_some {
  [] <> (eats[0] || eats[1] || eats[2] || eats[3] || eats[4])
}

ltl ltl_two {
  [] <> (eats[0] && eats[2])
}
