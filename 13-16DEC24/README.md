# Thirteenth class, 16 December 2024

Parity games. Connection to parity tree automata: there is a game in which player 0 wins if and only if the tree is accepted.

Corollary: parity tree languages are closed under complements. (proof uses memoryless strategy for player 1.)

Finkbeiner chapter 10.
